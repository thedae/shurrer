package profiler

import "time"

type Profiler struct {
	Start int64
	End   int64
}

func NewProfiler() *Profiler {
	now := time.Now().UnixNano()

	return &Profiler{Start: now, End: now}
}

func (p *Profiler) Stop() *Profiler {
	p.End = time.Now().UnixNano()
	return p
}

func (p Profiler) Delta() int64 {
	return p.End - p.Start
}
