package shurrer

const (
	ExitInvalidEnv    int = 3
	ExitUnableToBind  int = 4
	ExitInvalidConfig int = 5
)

const (
	ErrorLevelWarning  string = "WARN"
	ErrorLevelCritical string = "CRIT"
)

const DefaultMultiRequestTimeout = 5
