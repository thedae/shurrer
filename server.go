package shurrer

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/thedae/shurrer/profiler"
	"github.com/joho/godotenv"
)

type Shurrer struct {
	router         	*Router
	appEnvironment 	string
	globalHeader   	map[string]string
	httpServer 		*http.Server
}

type ServerLog struct {
	Status       int    `json:"status"`
	AppEnv       string `json:"app_env"`
	Delta        int64  `json:"delta_nano"`
	RemoteHost   string `json:"remote_host"`
	RemotePort   string `json:"remote_port"`
	RealClientIp string `json:"real_client_ip"`
	Method       string `json:"method"`
	Path         string `json:"path"`
	Query        string `json:"query"`
	RouterMatch  string `json:"router_match"`
	Referer      string `json:"referer"`
	UserAgent    string `json:"user_agent"`
	BodyBytes    int    `json:"body_bytes"`
	Error        string `json:"error,omitempty"`
}

func Default() (*Shurrer, *ServerError) {
	return newShurrer(), nil
}

func UsingEnv(environmentFile string) (*Shurrer, *ServerError) {
	if environmentFile == "" {
		return nil, NewServerError(ErrorLevelCritical, "env", "Given .env path should not be empty", ExitInvalidEnv)
	}

	fileError := godotenv.Load(environmentFile)
	if fileError != nil {
		return nil, NewServerError(ErrorLevelCritical, "env", "Error opening .env file: "+fileError.Error(), ExitInvalidEnv)
	}

	return newShurrer(), nil
}

func newShurrer() *Shurrer {
	object := &Shurrer{
		router:         NewRouter(),
		appEnvironment: "prod",
		globalHeader:   make(map[string]string),
	}

	return object
}

func (s *Shurrer) SetAppEnvironment(env string) *Shurrer {
	s.appEnvironment = env
	return s
}

func (s Shurrer) Run(address *string) *ServerError {
	var realAddress string
	if address == nil {
		realAddress = os.Getenv("LISTEN_ADDRESS")
	} else {
		realAddress = *address
	}
	if realAddress == "" {
		return NewServerError(ErrorLevelCritical, "listen", "Listen address cannot be empty", ExitUnableToBind)
	}

	http.HandleFunc("/", s.mainHandler)
	s.httpServer = &http.Server{Addr: realAddress}
	return s.serve()
}

func (s Shurrer) serve() *ServerError {
	listenError := make(chan *ServerError, 1)
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		err := s.httpServer.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			listenError <- NewServerError(ErrorLevelCritical, "listen", "Error binding port: "+err.Error(), ExitUnableToBind)
		}
	}()

	serveLoop:
	for {
		select {
		case le := <-listenError:
			return le
		case <-shutdown:
			break serveLoop
		}
	}
	return nil
}

func (s Shurrer) Shutdown() {
}

func (s *Shurrer) SetGlobalHeader(key, value string) {
	s.globalHeader[key] = value
}

func (s *Shurrer) Register(route Route) *ServerError {
	if registerErr := s.router.AddRoute(route); registerErr != nil {
		return NewServerError(ErrorLevelCritical, "register", "Method register error: "+registerErr.Error(), ExitInvalidConfig)
	}
	return nil
}

func (s *Shurrer) RegisterNotFound(ctrl Controller) {
	s.router.SetDefault("NotFound", ctrl)
}

func (s *Shurrer) RegisterMethodNotAllowed(ctrl Controller) {
	s.router.SetDefault("MethodNotAllowed", ctrl)
}

func (s Shurrer) mainHandler(w http.ResponseWriter, r *http.Request) {
	prof := profiler.NewProfiler()
	request, _ := s.process(r)

	match := s.router.Match(request.GetString("_Method"), request.GetString("_Path"))
	for k, v := range match.GetParameters() {
		request.Set(k, v)
	}

	var response ResponseInterface
	if !match.IsNested() {
		ctrl, _ := match.GetController()
		response = ctrl(request)
	} else {
		response = s.multiRequest(match.GetControllers(), request)
	}

	for hk, hv := range s.globalHeader {
		w.Header().Add(hk, hv)
	}
	w.Header().Add("Content-Type", response.GetMime()+"; charset=utf-8")
	w.WriteHeader(response.GetStatus())

	length, _ := w.Write([]byte(response.Output()))

	s.log(request, response, match, length, prof)
}

func (s Shurrer) multiRequest(controllers map[string]Controller, r *Request) ResponseInterface {
	workers := len(controllers)
	responseChannel := make(chan ResponseInterface, workers)
	responseParts := map[string]ResponseInterface{}

	for _, ctrl := range controllers {
		go func(c Controller) {
			responseChannel <- c(r)
		}(ctrl)
	}

	for i := range controllers {
		select {
		case responseParts[i] = <-responseChannel:
		case <-time.After(DefaultMultiRequestTimeout * time.Second):
			return JsonErrorResponse(http.StatusRequestTimeout, "Multi Request Timeout", "Multi Request Timeout")
		}
	}

	return JsonResponse(responseParts)
}

func (s Shurrer) process(r *http.Request) (*Request, error) {
	if err := r.ParseForm(); err != nil {
		return nil, err
	}

	request := NewRequest(s.appEnvironment, s.router)
	for k, v := range r.Form {
		request.Set(k, strings.Join(v, ""))
	}
	request.Set("_Path", r.URL.EscapedPath())
	request.Set("_Method", r.Method)
	request.Set("_Referer", r.Referer())
	request.Set("_UserAgent", r.UserAgent())
	request.Set("_RemoteAddr", r.RemoteAddr)
	request.Set("_ProxiedRemoteAddr", r.Header.Get("X-Real-IP"))

	return request, nil
}

func (s Shurrer) log(request *Request, response ResponseInterface, routerMatch *RouterMatch, bodyLength int, profiler *profiler.Profiler) {
	remoteIp, remotePort, _ := net.SplitHostPort(request.GetString("_RemoteAddr"))
	responseLog := ServerLog{
		Status:       response.GetStatus(),
		AppEnv:       s.appEnvironment,
		Delta:        0,
		RemoteHost:   remoteIp,
		RemotePort:   remotePort,
		RealClientIp: request.GetString("_ProxiedRemoteAddr"),
		Method:       request.GetString("_Method"),
		Path:         request.GetString("_Path"),
		Query:        request.String(),
		Referer:      request.GetString("_Referer"),
		UserAgent:    request.GetString("_UserAgent"),
		BodyBytes:    bodyLength,
		RouterMatch:  routerMatch.GetID(),
	}
	if response.GetError() != nil {
		responseLog.Error = response.GetError().Error()
	}

	responseLog.Delta = profiler.Stop().Delta()

	logPayload, _ := json.Marshal(responseLog)
	log.Println(string(logPayload))
}
