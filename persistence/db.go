package persistence

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type DatabaseConfig struct {
	User     string
	Password string
	Host     string
	Port     string
	Database string
	Charset  string
}

type Database struct {
	Connection *sql.DB
}

var NoResultsError error = sql.ErrNoRows
var ScanBlackhole interface{}

const DateTimeFormat string = "2006-01-02 15:04:05"

var instance *Database
var once sync.Once

func DateTimeNow() string {
	return time.Now().Format(DateTimeFormat)
}

func GetDb() *Database {
	once.Do(func() {
		dbConfig := &DatabaseConfig{
			User:     os.Getenv("DB_USER"),
			Password: os.Getenv("DB_PASSWORD"),
			Host:     os.Getenv("DB_HOST"),
			Port:     os.Getenv("DB_PORT"),
			Database: os.Getenv("DB_DEFAULT_DATABASE"),
			Charset:  os.Getenv("DB_DEFAULT_CHARSET"),
		}

		instance = &Database{}

		connection, err := sql.Open("mysql", dbConfig.GetAddress())
		if err != nil {
			log.Fatal("Could not initialize database:", err)
		}
		instance.Connection = connection
	})

	return instance
}

func (dc DatabaseConfig) GetAddress() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=false", dc.User, dc.Password, dc.Host, dc.Port, dc.Database, dc.Charset)
}
