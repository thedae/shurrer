package shurrer

import "fmt"

type BasicError struct {
	Level   string
	Context string
	Message string
}

type ServerError struct {
	*BasicError
	Code int
}

type RouterError struct {
	*BasicError
}

type ResponseError struct {
	*BasicError
}

func NewBasicError(level, context, message string) *BasicError {
	return &BasicError{
		Level:   level,
		Context: context,
		Message: message,
	}
}

func NewServerError(level, context, message string, code int) *ServerError {
	return &ServerError{
		BasicError: NewBasicError(level, context, message),
		Code:       code,
	}
}

func NewRouterError(level, context, message string) *RouterError {
	return &RouterError{BasicError: NewBasicError(level, context, message)}
}

func NewResponseError(level, context, message string) *ResponseError {
	return &ResponseError{BasicError: NewBasicError(level, context, message)}
}

func (e ServerError) Error() string {
	return fmt.Sprintf("[%s] server.%s code=%d %s", e.Level, e.Context, e.Code, e.Message)
}

func (e RouterError) Error() string {
	return fmt.Sprintf("[%s] router.%s %s", e.Level, e.Context, e.Message)
}

func (e ResponseError) Error() string {
	return fmt.Sprintf("[%s] response.%s %s", e.Level, e.Context, e.Message)
}
