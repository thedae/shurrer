package shurrer_test

import (
	"fmt"
	"regexp"
	"testing"

	"bitbucket.org/thedae/shurrer"
)

func TestCompile(t *testing.T) {
	var provider = []struct {
		input    string
		expected *regexp.Regexp
	}{
		{"/", extract("(?i)^/$")},
		{"/path", extract("(?i)^/path$")},
		{"/path/:id", extract("(?i)^/path/(?P<id>[-\\w]+)$")},
		{"/path/:id/other.json", extract("(?i)^/path/(?P<id>[-\\w]+)/other\\.json$")},
		{"/path/:id/:name/action-test.json", extract("(?i)^/path/(?P<id>[-\\w]+)/(?P<name>[-\\w]+)/action-test\\.json$")},
	}

	router := shurrer.NewRouter()
	for _, entry := range provider {
		testName := fmt.Sprintf("input=%s", entry.input)
		t.Run(testName, func(t *testing.T) {
			compiled := router.CompilePattern(entry.input)
			if compiled.String() != entry.expected.String() {
				t.Errorf("input=%s | compiled=%s | expected=%s", entry.input, compiled.String(), entry.expected.String())
			}
		})
	}
}

func TestGenerate(t *testing.T) {
	router := shurrer.NewRouter()

	entries := []shurrer.Route{
		{ID: "Default", Method: "GET", Pattern: "/", Controller: nil},
		{ID: "GetExample", Method: "GET", Pattern: "/example/:idExample", Controller: nil},
		{ID: "AnotherGetExample", Method: "GET", Pattern: "/example/:idExample/:otherId", Controller: nil},
		{ID: "CollisionExample", Method: "GET", Pattern: "/example/:idExample/:idExample2", Controller: nil},
		{ID: "PostExample", Method: "POST", Pattern: "/example", Controller: nil},

		{ID: "MultiExample", Method: "GET", Pattern: "/multi", Nested: []string{"Default", "GetExample"}},
	}

	for _, entry := range entries {
		if configErr := router.AddRoute(entry); configErr != nil {
			t.Errorf("failed to AddRoute(%s)", entry.ID)
		}
	}

	var provider = []struct {
		id    string
		params map[string]string
		expected string
	}{
		{"Default", map[string]string{}, "/"},
		{"GetExample", map[string]string{"idExample": "500-4"}, "/example/500-4"},
		{"AnotherGetExample", map[string]string{"idExample": "500-4", "otherId": "999"}, "/example/500-4/999"},
		{"CollisionExample", map[string]string{"idExample": "500-4", "idExample2": "999"}, "/example/500-4/999"},
		{"MultiExample", map[string]string{}, "/multi"},
	}

	for _, entry := range provider {
		testName := fmt.Sprintf("input=%s params=%v", entry.id, entry.params)
		t.Run(testName, func(t *testing.T) {
			route, _ := router.Generate(entry.id, entry.params)
			if route != entry.expected {
				t.Errorf("input=%s | params=%v | gotten=%s | expected=%s", entry.id, entry.params, route, entry.expected)
			}
		})
	}
}

func extract(pattern string) *regexp.Regexp {
	r, _ := regexp.Compile(pattern)
	return r
}
