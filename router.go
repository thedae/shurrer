package shurrer

import (
	"fmt"
	"regexp"
	"strings"
)

type Route struct {
	ID         string
	Method     string
	Pattern    string
	Controller Controller
	Nested     []string
}

type Router struct {
	entries  map[string]map[string]*RouterCompiledEntry
	defaults map[string]Controller
}

type RouterCompiledEntry struct {
	id              string
	pattern         string
	compiledPattern *regexp.Regexp
	controllers     map[string]Controller
}

type RouterMatch struct {
	id          string
	controllers map[string]Controller
	parameters  map[string]string
}

func NewRouter() *Router {
	entries := make(map[string]map[string]*RouterCompiledEntry)
	entries["GET"] = make(map[string]*RouterCompiledEntry)
	entries["POST"] = make(map[string]*RouterCompiledEntry)
	entries["PUT"] = make(map[string]*RouterCompiledEntry)
	entries["PATCH"] = make(map[string]*RouterCompiledEntry)
	entries["DELETE"] = make(map[string]*RouterCompiledEntry)

	defaults := make(map[string]Controller)
	defaults["NotFound"] = DefaultNotFoundController
	defaults["MethodNotAllowed"] = DefaultMethodNotAllowedController
	defaults["InternalServerError"] = DefaultServerErrorController
	defaults["Forbidden"] = DefaultForbiddenController

	return &Router{entries: entries, defaults: defaults}
}

func (r *Router) SetDefault(key string, ctrl Controller) {
	r.defaults[key] = ctrl
}

func (r *Router) AddRoute(route Route) error {
	_, exists := r.entries[route.Method]
	if !exists {
		return NewRouterError(ErrorLevelCritical, "AddRoute", "Method '"+route.Method+"' not allowed")
	}

	compiled := r.CompilePattern(route.Pattern)
	entry := &RouterCompiledEntry{
		id:              route.ID,
		pattern:         route.Pattern,
		compiledPattern: compiled,
		controllers:     make(map[string]Controller),
	}

	if route.Controller != nil {
		entry.controllers[route.ID] = route.Controller
	}

	if route.Nested != nil {
		for _, n := range route.Nested {
			e, exists := r.entries[route.Method][n]
			if !exists {
				return NewRouterError(ErrorLevelCritical, "AddRoute", "Nested route ID '"+n+"' is not registered just yet")
			}

			entry.controllers[n] = e.controllers[n]
		}
	}

	r.entries[route.Method][route.ID] = entry

	return nil
}

func (r Router) GetEntries() map[string]map[string]*RouterCompiledEntry {
	return r.entries
}

func (r Router) GetEntriesForMethod(method string) (map[string]*RouterCompiledEntry, error) {
	entries, exists := r.entries[method]
	if !exists {
		return nil, NewRouterError(ErrorLevelCritical, "GetEntriesForMethod", "Method '"+method+"' not implemented")
	}
	return entries, nil
}

func (r Router) Match(method, path string) *RouterMatch {
	params := make(map[string]string)

	routerEntries, routerError := r.GetEntriesForMethod(method)
	if routerError != nil {
		return &RouterMatch{
			controllers: map[string]Controller{"MethodNotAllowed": r.defaults["MethodNotAllowed"]},
			parameters:  params,
			id:          method + ",MethodNotAllowed",
		}
	}
	for _, entry := range routerEntries {
		matches := entry.compiledPattern.FindStringSubmatch(path)
		if len(matches) > 0 {
			for i, name := range entry.compiledPattern.SubexpNames() {
				if i != 0 && name != "" {
					params[name] = matches[i]
				}
			}

			return &RouterMatch{
				controllers: entry.controllers,
				parameters:  params,
				id:          method + "," + entry.id,
			}
		}
	}

	return &RouterMatch{
		controllers: map[string]Controller{"NotFound": r.defaults["NotFound"]},
		parameters:  params,
		id:          method + ",NotFound",
	}
}

func (r Router) CompilePattern(pattern string) *regexp.Regexp {
	parts := strings.Split(pattern, "/")
	compiledParts := make([]string, 0)
	for _, p := range parts {
		if len(p) > 0 && p[0] == ':' {
			p = "(?P<" + regexp.QuoteMeta(p[1:]) + ">[-\\w]+)"
		} else {
			p = regexp.QuoteMeta(p)
		}
		compiledParts = append(compiledParts, p)
	}

	compiled, _ := regexp.Compile("(?i)^" + strings.Join(compiledParts, "/") + "$")
	return compiled
}

func (r Router) Generate(id string, params map[string]string) (string, error) {
	for _, methods := range r.entries {
		for entryId, entry := range methods {
			if entryId == id {
				pattern := entry.pattern
				for key, val := range params {
					pattern = strings.Replace(pattern, ":" + key, val, 1)
				}
				return pattern, nil
			}
		}
	}
	return "", NewRouterError(ErrorLevelWarning, "Router.Generate", fmt.Sprintf("The given route ID was not found: %s", id))
}

func (rm RouterMatch) GetID() string {
	return rm.id
}

func (rm RouterMatch) GetParameters() map[string]string {
	return rm.parameters
}

func (rm RouterMatch) GetControllers() map[string]Controller {
	return rm.controllers
}

func (rm RouterMatch) GetController() (Controller, error) {
	if rm.IsNested() {
		return nil, NewRouterError(ErrorLevelCritical, "RouterMatch.GetController", "This is a nested request, cannot get main controller")
	}
	for _, v := range rm.controllers {
		return v, nil
	}
	return nil, NewRouterError(ErrorLevelCritical, "RouterMatch.GetController", "No defined controller")
}

func (rm RouterMatch) IsNested() bool {
	return len(rm.controllers) > 1
}
