package main

import (
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/thedae/shurrer"
)

func main() {
	server, err := shurrer.Default()
	if err != nil {
		fmt.Println(err)
		os.Exit(err.Code)
	}
	defer server.Shutdown()

	entries := []shurrer.Route{
		{ID: "Default", Method: "GET", Pattern: "/", Controller: Default},
		{ID: "GetExample", Method: "GET", Pattern: "/example/:idExample", Controller: GetExample},
		{ID: "PostExample", Method: "POST", Pattern: "/example", Controller: PostExample},

		{ID: "MultiExample", Method: "GET", Pattern: "/multi", Nested: []string{"Default", "GetExample"}},
	}

	for _, entry := range entries {
		if configErr := server.Register(entry); configErr != nil {
			fmt.Println(configErr)
			os.Exit(configErr.Code)
		}
	}

	server.SetGlobalHeader("Access-Control-Allow-Origin", "*")
	server.RegisterNotFound(notFound)
	server.RegisterMethodNotAllowed(methodNotAllowed)

	address := ":9030"
	runError := server.Run(&address)
	if runError != nil {
		fmt.Println(runError)
		os.Exit(runError.Code)
	}
}

func Default(r *shurrer.Request) shurrer.ResponseInterface {
	defaultPayload := make(map[string]string)
	defaultPayload["version"] = "v0.4.0"

	return shurrer.JsonResponse(defaultPayload)
}

func GetExample(r *shurrer.Request) shurrer.ResponseInterface {
	var exampleEntity = []struct {
		ID     	int    `json:"id"`
		Name   	string `json:"name"`
		Secret 	string `json:"-"`
		Rel 	string `json:"rel"`
	}{
		{r.GetInt("idExample"), "Example", "ExampleSecret", r.GenerateRoute("GetExample", map[string]string{"idExample": r.GetString("idExample")})},
	}

	return shurrer.JsonResponse(exampleEntity)
}

func PostExample(r *shurrer.Request) shurrer.ResponseInterface {
	response := shurrer.JsonResponse("")
	response.SetStatus(http.StatusCreated)

	return response
}

func notFound(r *shurrer.Request) shurrer.ResponseInterface {
	return shurrer.JsonErrorResponse(http.StatusNotFound, "Not found", "Not found")
}

func methodNotAllowed(r *shurrer.Request) shurrer.ResponseInterface {
	return shurrer.JsonErrorResponse(http.StatusMethodNotAllowed, "Method not allowed", "Method not allowed")
}
