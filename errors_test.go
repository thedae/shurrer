package shurrer_test

import (
	"fmt"
	"testing"

	"bitbucket.org/thedae/shurrer"
)

func TestErrorOutput(t *testing.T) {
	var provider = []struct {
		input    error
		expected string
	}{
		{
			shurrer.NewServerError(shurrer.ErrorLevelCritical, "test", "Test message", 1),
			"[CRIT] server.test code=1 Test message",
		},
		{
			shurrer.NewRouterError(shurrer.ErrorLevelWarning, "test", "Test message"),
			"[WARN] router.test Test message",
		},
		{
			shurrer.NewResponseError(shurrer.ErrorLevelCritical, "test", "Test message"),
			"[CRIT] response.test Test message",
		},
	}

	for _, entry := range provider {
		testName := fmt.Sprintf("input=%s", entry.input)
		t.Run(testName, func(t *testing.T) {
			if entry.input.Error() != entry.expected {
				t.Errorf("input=%s | expected=%s", entry.input.Error(), entry.expected)
			}
		})
	}

}
