package shurrer

import (
	"crypto/sha256"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

func Sha256String(s ...string) (string, error) {
	sha := sha256.New()
	_, err := sha.Write([]byte(strings.Join(s, "")))
	if err != nil {
		return "", err
	}
	hash := sha.Sum(nil)

	return fmt.Sprintf("%x", hash), nil
}

func Sha256Now() (string, error) {
	currentTime := strconv.FormatInt(time.Now().Unix(), 10)
	sha := sha256.New()
	_, err := sha.Write([]byte(currentTime))
	if err != nil {
		return "", err
	}
	hash := sha.Sum(nil)

	return fmt.Sprintf("%x", hash), nil
}

func Secret(args ...string) string {
	a := make([]string, len(args)+1)
	copy(a, args)
	a[len(args)] = os.Getenv("SECRET")

	hash, _ := Sha256String(a...)

	return hash
}
