package shurrer

import (
	"net/http"
)

type Controller func(*Request) ResponseInterface

var DefaultNotFoundController Controller = func(r *Request) ResponseInterface {
	return DefaultErrorResponse(http.StatusNotFound, "Not found", "")
}

var DefaultMethodNotAllowedController Controller = func(r *Request) ResponseInterface {
	return DefaultErrorResponse(http.StatusMethodNotAllowed, "Method not allowed", "")
}

var DefaultForbiddenController Controller = func(r *Request) ResponseInterface {
	return DefaultErrorResponse(http.StatusForbidden, "Forbidden", "")
}

var DefaultServerErrorController Controller = func(r *Request) ResponseInterface {
	return DefaultErrorResponse(http.StatusInternalServerError, "Internal server error", "")
}
