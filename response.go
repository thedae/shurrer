package shurrer

import (
	"encoding/json"
	"errors"
	"net/http"
)

type BasicResponse struct {
	Status int    `json:"status"`
	Mime   string `json:"-"`
	Body   string `json:"-"`
}

type ErrorResponse struct {
	*BasicResponse
	Internal error `json:"-"`
}

type Plain struct {
	*BasicResponse
}

type Json struct {
	*BasicResponse
	Body interface{} `json:"items"`
}

type PlainError struct {
	*ErrorResponse
}

type JsonError struct {
	*ErrorResponse
	Body interface{} `json:"message"`
}

type ResponseInterface interface {
	SetStatus(status int)
	SetMime(mime string)
	SetBody(body string)
	GetStatus() int
	GetMime() string
	GetBody() string
	GetError() error

	Output() string
}

func (r *BasicResponse) SetStatus(status int) {
	r.Status = status
}

func (r *BasicResponse) SetMime(mime string) {
	r.Mime = mime
}

func (r *BasicResponse) SetBody(body string) {
	r.Body = body
}

func (r BasicResponse) GetBody() string {
	return r.Body
}

func (r BasicResponse) GetMime() string {
	return r.Mime
}

func (r BasicResponse) GetStatus() int {
	return r.Status
}

func (r BasicResponse) Output() string {
	return r.Body
}

func (r Json) Output() string {
	output, _ := json.Marshal(r)

	return string(output)
}

func (r JsonError) Output() string {
	output, _ := json.Marshal(r)

	return string(output)
}

func (r BasicResponse) GetError() error {
	return nil
}

func (r ErrorResponse) GetError() error {
	return r.Internal
}

func DefaultResponse() *BasicResponse {
	return &BasicResponse{
		Status: http.StatusOK,
		Mime:   "text/plain",
		Body:   "",
	}
}

func PlainResponse(body string) *Plain {
	response := &Plain{
		BasicResponse: DefaultResponse(),
	}
	response.SetBody(body)
	return response
}

func JsonResponse(body interface{}) *Json {
	response := &Json{
		BasicResponse: DefaultResponse(),
		Body:          body,
	}
	response.BasicResponse.SetMime("application/json")
	return response
}

func DefaultErrorResponse(status int, message, internal string) *ErrorResponse {

	response := &ErrorResponse{
		BasicResponse: DefaultResponse(),
		Internal:      errors.New(internal),
	}
	response.BasicResponse.SetStatus(status)
	response.BasicResponse.SetBody(message)
	return response
}

func JsonErrorResponse(status int, body interface{}, internal string) *JsonError {
	response := &JsonError{
		ErrorResponse: DefaultErrorResponse(status, "", internal),
		Body:          body,
	}
	response.ErrorResponse.BasicResponse.SetMime("application/json")
	return response
}
