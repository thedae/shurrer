package shurrer

import (
	"strconv"
	"strings"
)

type Request struct {
	params         	map[string]string
	appEnvironment 	string
	router	 		*Router
}

func NewRequest(appEnv string, router *Router) *Request {
	p := &Request{appEnvironment: appEnv, router: router}
	p.params = make(map[string]string)

	return p
}

func (p Request) IsDev() bool {
	return p.appEnvironment == "dev"
}

func (p Request) String() string {
	result := make([]string, 0)
	for k, v := range p.params {
		if k[0] != '_' {
			result = append(result, k+"="+v)
		}
	}
	return strings.Join(result, "&")
}

func (p *Request) Set(key string, value string) {
	p.params[key] = value
}

func (p Request) GetString(key string) string {
	if val, exists := p.params[key]; exists {
		return val
	}
	return ""
}

func (p Request) GetInt(key string) int {
	if val, exists := p.params[key]; exists {
		number, _ := strconv.Atoi(val)
		return number
	}
	return 0
}

func (p Request) Exists(key string) bool {
	_, exists := p.params[key]

	return exists
}

func (p Request) Empty(key string) bool {
	return !p.Exists(key) || p.GetString(key) == ""
}

func (p Request) GenerateRoute(id string, params map[string]string) string {
	route, err := p.router.Generate(id, params)
	if err != nil {
		return ""
	}
	return route
}
